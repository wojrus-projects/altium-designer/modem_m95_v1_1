# Specyfikacja

Urządzenie jest modemem GSM/GPRS podłączanym do PC przez USB 2.0.

- Bazuje na module GSM/GPRS Quectel M95 (https://www.quectel.com/product/gsm-gprs-m95)
  - Quad-band: 850 / 900 / 1800 / 1900 MHz
  - GPRS Mobile Station: Class B
  - GPRS Class 12 (1-12): Max. 85.6 kbps (Uplink & Downlink)
  - Coding Schemes: CS 1, 2, 3, 4
  - USSD
  - SMS Text and PDU Mode
  - Speech Codec Modes: HR/FR
  - Output Power: Class 4 (2 W @ 900 MHz), Class 1 (1 W @ 1800 MHz)
  - Protocols: PPP, TCP, UDP, FTP, HTTP, SMTP, SSL
  - AT commands
- Zasilanie zewnętrzne: +9...+24 V DC / 1 A maks.
  - Gniazdo zasilania: power jack D=5,5 mm / pin D=2,0 mm
- Interfejs komunikacyjny USB 2.0:
  - Gniazdo B micro
  - Wirtualny RS232 bazujący na FT232R
    - Flow control RTS/CTS
  - Funkcja usypiania (USB suspend) wyłączająca zasilanie modemu
  - Rozkazy AT opisane są w dokumencie "Quectel_M95_AT_Commands_Manual_V3.2".
- Złącze SMA anteny zewnętrznej
- Gniazdo karty micro-SIM push-pull
- Gniazdo audio jack 3,5 mm 4-pin
  - Standard pinów zgodny z headsets kompatybilnych z telefonami Android
- Przycisk "EMERG-OFF" do natychmiastowego wyłączenia modemu
- Przycisk "PWR-KEY" do kontrolowanego włączania i wyłączania modemu (z wylogowaniem z sieci GSM)
- 4 LEDy statusu:
  - <span style="color:green">"PWR-USB" zielona</span>: stan zasilania i uśpienia USB
  - <span style="color:orange">"STATUS" pomarańczowa</span>: stan uruchomienia modemu
  - <span style="color:red">"NETWORK" czerwona</span>: stan komunikacji z siecią GSM
  - <span style="color:green">"PWR-GSM" zielona</span>: stan zasilania modemu
- Zworka PCB do automatycznego włączania zasilania modemu
- Zworka PCB do zdalnego włączania/wyłączania zasilania modemu za pośrednictwem sygnału RS232-DTR (wymaga dedykowanego oprogramowania)
- Obudowa nabiurkowa 75 mm x 74 mm x 27 mm

# Projekt PCB

Schemat: [altium/doc/MODEM_M95_V1_1_SCH.pdf](altium/doc/MODEM_M95_V1_1_SCH.pdf)

Widok 3D: [altium/doc/MODEM_M95_V1_1_3D.pdf](altium/doc/MODEM_M95_V1_1_3D.pdf) (wymaga *Adobe Acrobat Reader DC*)

![](resource/pcb_3d.png "PCB 3D")

![](resource/pcb.png "PCB top")

# Licencja

MIT
